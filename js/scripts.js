let refresh = document.querySelector("#clear")
let dateElement = document.querySelector("#date")
let list = document.querySelector("#list")
let input = document.querySelector("#input")
let addIcon = document.querySelector("#add-icon")

// CODE EXPLAINED channel


// Classes names from Front-Awesome
const CHECK = "fa-check-circle" // green checked icon
const UNCHECK = "fa-circle-thin" // empty circled icon
const LINE_THROUGH = "lineThrough" // dash out the data

let LIST, id;

// get item from localstorage
let data = localStorage.getItem("TODO")

// check if data is not empty
if(data){

    LIST = JSON.parse(data)
    id = LIST.length 
    loadList(LIST) 

}else{ //localStorage is empty

    LIST = []
    id = 0

}

function loadList(list){
    list.forEach(function(listItem){
        addToDo(listItem.name, listItem.id, listItem.done, listItem.trash)
    });
}

// clear the localStorage when pressing the clear btn
clear.addEventListener("click", function(){
    localStorage.clear()
    location.reload()
});


//displaying the date
const options = {weekday : "long", month:"short", day:"numeric"}
const today = new Date()

dateElement.innerHTML = today.toLocaleDateString("en-US", options)



//this function adds a task to the html page and adds it to the localStorage
function addToDo(task, id, done, trash){
    
    if(trash){ return }
    
    var DONE = done ? CHECK : UNCHECK // add the check class if the task is checked
    var LINE = done ? LINE_THROUGH : "" // add the linethrough class is the task is checked
    
    const item = `<li class="item">
                    <i class="fa ${DONE} co" job="complete" draggable = "true" id="${id}"></i>
                    <p class="text ${LINE}">${task}</p>
                    <i class="fa fa-trash-o de" job="delete" id="${id}"></i>
                  </li>
                `
    
    const position = "afterbegin"
    
    //insert the item created at the top of the list
    list.insertAdjacentHTML(position, item)

}


// add an item to the list user the enter key
document.addEventListener("keyup",(e)=>{
    if(e.keyCode == 13){

        var task = input.value

        if(task){
            addToDo(task, id, false, false);
            
            LIST.push({
                name : task,
                id : id,
                done : false,
                trash : false //when creating a new task it is not done yet nor deleted
            })
            
            // update localStorage
            localStorage.setItem("TODO", JSON.stringify(LIST));
            
            id += 1 
        }
        input.value = ""
    }
})

//when pressing on add icon add the task
addIcon.addEventListener("click", function(){
    var task = input.value

        if(task){
            addToDo(task, id, false, false);
            
            LIST.push({
                name : task,
                id : id,
                done : false,
                trash : false //when creating a new task it is not done yet nor deleted
            })
            
            // update localStorage
            localStorage.setItem("TODO", JSON.stringify(LIST));
            
            id += 1 
        }
        input.value = ""
})






//click listener on the list element :

list.addEventListener("click", function(event){

    //get the element clicked
    var element = event.target

    //get the element job (complete or delete)
    var elementJob = element.attributes.job.value
    
    if(elementJob == "complete"){
        completeToDo(element)
    }else if(elementJob == "delete"){
        removeToDo(element)
    }
    
    // add item to localstorage ( this code must be added where the LIST array is updated)
    localStorage.setItem("TODO", JSON.stringify(LIST))
})



function completeToDo(element){

    //switch the click (checked, un-checked)
    element.classList.toggle(CHECK)
    element.classList.toggle(UNCHECK)

    //switch the dashed-out (on, off)
    element.parentNode.querySelector(".text").classList.toggle(LINE_THROUGH);
    
    LIST[element.id].done = LIST[element.id].done ? false : true;
}

function removeToDo(element){

    //get to the ul element and delete its child (li)
    element.parentNode.parentNode.removeChild(element.parentNode);
    
    LIST[element.id].trash = true;
    
}


// Mouaz - Al-Kassm













